use crate::C1Token::*;
use crate::{C1Lexer, C1Token, ParseResult};

pub struct C1Parser<'a> {
    lex: C1Lexer<'a>,
}

impl<'a> C1Parser<'a> {
    pub fn parse(text: &str) -> ParseResult {
        let mut parser = C1Parser::new(text);
        parser.program()
    }

    fn new(text: &'a str) -> C1Parser {
        C1Parser {
            lex: C1Lexer::new(text),
        }
    }

    fn eat(&mut self) {
        self.lex.eat()
    }

    fn line_err(&self, err: String) -> ParseResult {
        let e_line = match self.lex.current_line_number() {
            Some(x) => format!("Line {x}: "),
            None => "Unknown Line: ".to_string(),
        };
        Err(format!("{e_line}{err}"))
    }

    fn check_and_eat_token(&mut self, token: C1Token) -> ParseResult {
        match self.lex.current_token() {
            Some(current) if current == token => {
                self.eat();
                Ok(())
            }
            Some(current) => self.line_err(format!("Expected {token:?}, got {current:?}")),
            None => self.line_err(format!("Expected {token:?} but no tokens were left")),
        }
    }

    fn current_matches(&mut self, token: C1Token) -> bool {
        self.lex.current_token() == Some(token)
    }

    fn next_matches(&mut self, token: C1Token) -> bool {
        self.lex.peek_token() == Some(token)
    }

    fn program(&mut self) -> ParseResult {
        loop {
            match self.functiondefinition() {
                Ok(()) => (),
                e => {
                    return match self.lex.current_token() {
                        Some(_) => e,
                        None => Ok(()),
                    }
                }
            }
        }
    }

    fn functiondefinition(&mut self) -> ParseResult {
        self.type_()?;
        self.check_and_eat_token(Identifier)?;
        self.check_and_eat_token(LeftParenthesis)?;
        self.check_and_eat_token(RightParenthesis)?;
        self.check_and_eat_token(LeftBrace)?;
        self.statementlist()?;
        self.check_and_eat_token(RightBrace)
    }

    fn functioncall(&mut self) -> ParseResult {
        self.check_and_eat_token(Identifier)?;
        self.check_and_eat_token(LeftParenthesis)?;
        self.check_and_eat_token(RightParenthesis)
    }

    fn statementlist(&mut self) -> ParseResult {
        loop {
            match self.block() {
                Ok(()) => (),
                Err(_) => return Ok(()),
            }
        }
    }

    fn block(&mut self) -> ParseResult {
        if self.current_matches(LeftBrace) {
            self.eat();
            self.statementlist()?;
            self.check_and_eat_token(RightBrace)
        } else {
            self.statement()
        }
    }

    fn statement(&mut self) -> ParseResult {
        if self.current_matches(KwIf) {
            self.ifstatement()
        } else if self.current_matches(KwReturn) {
            self.returnstatement()?;
            self.check_and_eat_token(Semicolon)
        } else if self.current_matches(KwPrintf) {
            self.printf()?;
            self.check_and_eat_token(Semicolon)
        } else if self.current_matches(Identifier) {
            if self.next_matches(Assign) {
                self.assignment()?;
                self.check_and_eat_token(Semicolon)
            } else if self.next_matches(LeftParenthesis) {
                self.functioncall()?;
                self.check_and_eat_token(Semicolon)
            } else {
                self.line_err(format!("statement"))
            }
        } else {
            self.line_err(format!("statement"))
        }
    }

    fn ifstatement(&mut self) -> ParseResult {
        self.check_and_eat_token(KwIf)?;
        self.check_and_eat_token(LeftParenthesis)?;
        self.assignment()?;
        self.check_and_eat_token(RightParenthesis)?;
        self.block()
    }

    fn returnstatement(&mut self) -> ParseResult {
        self.check_and_eat_token(KwReturn)?;
        if self.current_matches(Identifier)
            || self.current_matches(Minus)
            || self.current_matches(ConstInt)
            || self.current_matches(ConstFloat)
            || self.current_matches(ConstBoolean)
            || self.current_matches(LeftParenthesis)
        {
            self.assignment()
        } else {
            Ok(())
        }
    }

    fn printf(&mut self) -> ParseResult {
        self.check_and_eat_token(KwPrintf)?;
        self.check_and_eat_token(LeftParenthesis)?;
        self.assignment()?;
        self.check_and_eat_token(RightParenthesis)
    }

    fn type_(&mut self) -> ParseResult {
        if self.current_matches(KwBoolean)
            || self.current_matches(KwFloat)
            || self.current_matches(KwInt)
            || self.current_matches(KwVoid)
        {
            self.eat();
            Ok(())
        } else {
            self.line_err(format!("type"))
        }
    }

    fn statassignment(&mut self) -> ParseResult {
        self.check_and_eat_token(Identifier)?;
        self.check_and_eat_token(Assign)?;
        self.assignment()
    }

    fn assignment(&mut self) -> ParseResult {
        if self.current_matches(Identifier) && self.next_matches(Assign) {
            self.statassignment()
        } else {
            self.expr()
        }
    }

    fn expr(&mut self) -> ParseResult {
        self.simpexpr()?;
        if self.current_matches(Equal)
            || self.current_matches(NotEqual)
            || self.current_matches(LessEqual)
            || self.current_matches(GreaterEqual)
            || self.current_matches(Less)
            || self.current_matches(Greater)
        {
            self.eat();
            self.simpexpr()
        } else {
            Ok(())
        }
    }

    fn simpexpr(&mut self) -> ParseResult {
        if self.current_matches(Minus) {
            self.eat();
        }
        self.term()?;
        while self.current_matches(Plus) || self.current_matches(Minus) || self.current_matches(Or)
        {
            self.eat();
            self.term()?;
        }
        Ok(())
    }

    fn term(&mut self) -> ParseResult {
        self.factor()?;
        while self.current_matches(Asterisk)
            || self.current_matches(Slash)
            || self.current_matches(And)
        {
            self.eat();
            self.factor()?;
        }
        Ok(())
    }

    fn factor(&mut self) -> ParseResult {
        if self.current_matches(ConstInt)
            || self.current_matches(ConstFloat)
            || self.current_matches(ConstBoolean)
        {
            self.eat();
            Ok(())
        } else if self.current_matches(Identifier) {
            if self.next_matches(LeftParenthesis) {
                self.functioncall()
            } else {
                self.eat();
                Ok(())
            }
        } else if self.current_matches(LeftParenthesis) {
            self.eat();
            self.assignment()?;
            self.check_and_eat_token(RightParenthesis)
        } else {
            self.line_err(format!("factor"))
        }
    }
}
